package main

import "fmt"

const myformalgreeting = "Hello, "

func Hello(name string) string {
	if name == "" {
		name = "World"
	}
	return myformalgreeting + name
}

func main() {
	fmt.Println(Hello("Bob"))
}
